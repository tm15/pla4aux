package ga.manuelgarciacr.tripmemories.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "table_trips")
public class Trip implements Comparable<Trip>{
    @PrimaryKey
    @SerializedName("uuid")
    @NonNull
    private UUID uuid = java.util.UUID.randomUUID();
    @SerializedName("name")
    private String name;
    @SerializedName("country")
    private String country;
    @SerializedName("date")
    private Date date;
    @SerializedName("comp")
    private String companion;
    @SerializedName("photo")
    private String photo;
    @SerializedName("deleted")
    private String deleted;
    @SerializedName("phone")
    private String phone;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    private String email;

    @Ignore
    public Trip(@org.jetbrains.annotations.NotNull String name,
                String country,
                Date date,
                String companion,
                String photo,
                String deleted,
                String phone,
                double latitude,
                double longitude) {
        super();
        this.setName(name);
        this.setCountry(country);
        this.setDate(date);
        this.setCompanion(companion);
        this.setPhoto(photo);
        this.setDeleted(deleted);
        this.setPhone(phone);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public Trip(String name, String country) {
        this(name, country, new Date(), "", "", "0", "", 0, 0);
    }

    @NotNull
    public UUID getUuid() {
        return uuid;
    }

    void setUuid(@NotNull UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null)
            this.name = "";
        else
            this.name = name.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if(country == null)
            this.country = "";
        else
            this.country = country.trim();
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }

    public String getCompanion() {
        return companion;
    }

    public void setCompanion(String companion) {
        if(companion == null)
            this.companion = "";
        else
            this.companion = companion.trim();
    }

    public String getPhoto() {
        if(this.photo.equals(""))
            setPhoto("IMG_" + getUuid() + ".jpg");
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Trip getReplicate(){
        return new Trip(getName(), getCountry(), getDate(), getCompanion(), getPhoto(), getDeleted(), getPhone(), getLatitude(), getLongitude());
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + getDate().toString() + ": " + getName() + ", " + getCountry() + ", " + getCompanion() + ", " + getUuid() + "]";
    }

    @Override
    public int compareTo(Trip o) {
        return (this.getName().compareTo(o.getName()));
    }
}
