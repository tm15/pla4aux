package ga.manuelgarciacr.tripmemories.ui.map;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ga.manuelgarciacr.tripmemories.model.Request;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class MapViewModel extends AndroidViewModel {
    private final TripRepository mTripRepository;
    private LiveData<Request> mAllTrips = new MutableLiveData<>();

    public MapViewModel(Application application) {
        super(application);
        mTripRepository = TripRepository.getInstance(application);
    }

    public void allTrips() {
        mTripRepository.allTrips(new Request("list"), mAllTrips);
    }

    public LiveData<Request> getAllTrips() {
        return mAllTrips;
    }
}